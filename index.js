const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

const firestore = admin.firestore();

exports.onUserStatusChange = functions.database
    .ref("/{uid}/isOnline")
    .onUpdate(async (change, context) => {
    // Get the data written to Realtime Database
      const isOnline = change.after.val();

      // Get a reference to the Firestore document
      const userStatusFirestoreRef = firestore.doc(
          `ChatUsers/${context.params.uid}`
      );

      console.log(`isOnline: ${isOnline}`);

      // Update the values on Firestore
      return userStatusFirestoreRef.update({
        isOnline: isOnline,
        lastSeenTime: new Date().toISOString(),
      });
    });

exports.sendNotification = functions.firestore
    .document("ChatRooms/{chatRoomId}/Conversation/{conversationId}")
    .onCreate((snap, context) => {
      console.log("----------------start function--------------------");
      const doc = snap.data();
      console.log("UserName: ", doc["sender"]["username"]);

      const receiverId =
      context.params.chatRoomId
          .split("_")
          .find((item) => item != doc["sender"]["username"]);

      console.log("Receiver Identity: ", receiverId);
      // Get push token user to (receive)
      admin
          .firestore()
          .collection("ChatUsers")
          .doc(receiverId)
          .get()
          .then((docSnapshot) => {
            const data = docSnapshot.data();
            if (data !== null) {
              console.log(`Found user to: ${data["username"]}`);
              if (data["fcmToken"] !== null &&
                data["isLoggedIn"] == true &&
                data["isOnline"] == false) {
                const payload = {
                  tokens: data["fcmToken"],
                  data: {
                    body: doc["text"],
                    title: doc["sender"]["name"],
                    image: doc["sender"]["imageUrl"],
                    click_action: "FLUTTER_NOTIFICATION_CLICK",
                  },
                };
                admin
                    .messaging()
                    .sendMulticast(payload)
                    .then((response) => {
                      console.log(`Successfully sent message:
                                , Success Count: ${response.successCount}, 
                                Failure Count: ${response.failureCount}`);
                    })
                    .catch((error) => {
                      console.log("Error sending message:", error.message);
                    });
              } else {
                console
                    .log(`Can not find 
                    pushToken/isLoggedIn/isOnline
                     target user`);
              }
            } else {
              console.log("Can not find target user");
            }
          });
      return null;
    });
